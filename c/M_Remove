/*
 * Copyright (c) 2003, Rik Griffin
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
/* Tabs:M_Remove.c		*/
/* Rik Griffin Nov 2003		*/

//#include "MemCheck:MemCheck.h"


#include "kernel.h"
#include "swis.h"

#include "toolbox.h"
#include "window.h"
#include "wimplib.h"

#include "glib.h"

#include "Main.h"


_kernel_oserror *_remove(int recurse_flag, PrivateTabs *me) {

  IGNORE(recurse_flag);

  DEBUG(3, ("_remove: %x  recurse: %d\n", me, recurse_flag));

  /* remove the gadget being deleted from our list	*/
//  if ((e = remove_redraw_handler(me)) != NULL) return e;

  /* Get the toolbox to uninstall the post filters	*/
  remove_task_interest(GLib_WimpEvents, Filter_WimpEvents);
//  remove_task_interest(GLib_ToolboxEvents, Filter_ToolboxEvents);
  remove_task_interest(GLib_WimpMessages, Filter_WimpMessages);

  DEBUG(3, ("removed toolbox filters\n"));

  remove_tabs_from_list(me);

  /* delete the tabs data and attached objects		*/
  tabs_delete(me);

  /* delete our window	*/
  toolbox_delete_object(0, me->object_id);

  //MemCheck_UnRegisterMiscBlock(me);

  _mem_free(me);

  DEBUG(3, ("Deleted Tabs gadget handle %08x\n", me));

  return NULL;
}
